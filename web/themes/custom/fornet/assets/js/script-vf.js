// Uses CommonJS, AMD or browser globals to create a jQuery plugin.
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = function (root, jQuery) {
            if (jQuery === undefined) {
                // require('jQuery') returns a factory that requires window to
                // build a jQuery instance, we normalize how we use modules
                // that require this pattern but the window provided is a noop
                // if it's defined (how jquery works)
                if (typeof window !== 'undefined') {
                    jQuery = require('jquery');
                }
                else {
                    jQuery = require('jquery')(root);
                }
            }
            factory(jQuery);
            return jQuery;
        };
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    /**
     * jquery.fnm.nav.js
     * contain nav js
     */
    var fnmNav;

    fnmNav = {

        init: function () {

            AOS.init();

            var $window = $(window);

            // Add Link Href in section RÉALISATIONS
            $('.links--realisations').on('click', function (event) {
                var url = $(this).data('target');
                window.location.href = url;
                event.preventDefault();
            });

            // Add scrolle Agence
            $('.menu--scrolle ul a').on('click', function () {
                $('html, body').animate({
                    scrollTop: $(this.hash).offset().top - 50
                }, 500);
                return false;
            });

            // Slider Hp.
            $('#slider--video').owlCarousel({
                loop: false,
                touchDrag: false,
                mouseDrag: false,
                margin: 15,
                responsiveClass: true,
                nav: false,
                dots: false,
                autoplay: true,
                // center: true,
                navText: [
                    '<i class="fa-solid fa-angle-left"></i>',
                    '<i class="fa-solid fa-angle-right"></i>',
                ],
                autoplay: false,
                autoPlaySpeed: 2000,
                autoPlayTimeout: 2000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        items: 1.1,
                        margin: 10,
                        nav: false,
                        touchDrag: true,
                        mouseDrag: true,
                    },
                    600: {
                        items: 1,
                        nav: false,
                        touchDrag: true,
                        mouseDrag: true,
                    },
                    1000: {
                        items: 1,
                        nav: false,
                        loop: false,
                        autoplay: true,
                    }
                }
            });

            // Slider Réference.
            $('#box--réferences').owlCarousel({
                loop: true,
                // touchDrag: false,
                // mouseDrag: false,
                margin: 0,
                responsiveClass: true,
                nav: false,
                dots: true,
                // center: true,
                navText: [
                    '<i class="fa-solid fa-angle-left"></i>',
                    '<i class="fa-solid fa-angle-right"></i>',
                ],
                autoplay: false,
                autoPlaySpeed: 2000,
                autoPlayTimeout: 2000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        items: 1.1,
                        margin: 10,
                        dots: true,
                        nav: false,
                        // touchDrag: true,
                        // mouseDrag: true,
                    },
                    600: {
                        items: 2,
                        nav: false,
                        dots: true,
                        // touchDrag: true,
                        // mouseDrag: true,
                    },
                    1000: {
                        items: 4,
                        nav: false,
                        loop: true,
                        dots: true,
                    },
                    1300: {
                        items: 5,
                        nav: false,
                        loop: true,
                        dots: true,
                    }
                }
            });

            // Fixed menu  et icon bottom
            $window.bind("scroll", function () {
                if ($window.scrollTop() > 50) {
                    $(".menu-logo").addClass("menu--bar-fixed");
                    $('.action-gototop').addClass('is-visible');
                } else {
                    $(".menu-logo").removeClass("menu--bar-fixed");
                    $('.action-gototop').removeClass('is-visible');
                }
            });

            // QUI SOMMES NOUS ?
            $window.scroll(function () {
                if ($window.scrollTop() > 350) {
                    $('.qui-sommes--nous .counter').each(function () {
                        var count = $(this);
                        var countTo = count.attr('data-count');
                        $({
                            countNum: count.text()
                        }).animate({
                            countNum: countTo,
                        }, {
                            duration: 3000,
                            easing: 'linear',
                            step: function () {
                                count.text(Math.floor(this.countNum));
                            },
                            complete: function () {
                                count.text(this.countNum);
                            },
                        });
                    });
                }
                $('.counter--box--agence .counter').each(function () {
                    var count = $(this);
                    var countTo = count.attr('data-count');
                    $({
                        countNum: count.text()
                    }).animate({
                        countNum: countTo,
                    }, {
                        duration: 3000,
                        easing: 'linear',
                        step: function () {
                            count.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            count.text(this.countNum);
                        },
                    });
                });
            });


            // Show Search
            $("header .btn--search").on("click", function () {
                $("header .search--bar").slideToggle();
                $(".search--bar input").focus();
            });


            // Close Form Search
            $("header .search--bar button").on("click", function (e) {
                e.preventDefault();
                $("header .search--bar").slideToggle();
            });

            // Show popup video QUI SOMMES NOUS ?
            $(".cover--img-sommes").on('click', function (event) {
                $(".video-popup").show();
            })

            // Close popup video QUI SOMMES NOUS ?

            $(".popup-bg , .popup-photo-close").click(function (e) {
                $(".video-popup").fadeOut("slow");
            });

            // Tabs carde
            $('.list--nos--equipes button').eq(0).addClass('tab-active-bg');
            $('.list--nos--equipes button').on('click', function (e, index) {
                $('.list--nos--equipes button').each(function () {
                    $(this).removeClass('tab-active-bg');
                });
                $(this).addClass('tab-active-bg');
                // $(this+".infos-de-linsta-cirlce-h").css("background","#c8e6f5")
                let indexElment = $(this).index();
                $('.all-elements--list .element-equipe-').each(function () {
                    $(this).css('display', 'none');
                });
                $('.all-elements--list .element-equipe-').eq(indexElment).css('display', 'block');
            });

            // Click link add class
            $(".all--tabs--scrolle-agnece .menu--scrolle ul li:first-child").addClass("inclick-href");
            $('.all--tabs--scrolle-agnece .menu--scrolle ul li').on('click', function () {

                $(".all--tabs--scrolle-agnece .menu--scrolle ul li").removeClass("inclick-href");
                $(this).addClass("inclick-href");
            });
        }

    };

    $(document).ready(function () {
        fnmNav.init();
    });
}));
