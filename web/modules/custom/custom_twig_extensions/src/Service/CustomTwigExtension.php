<?php

/**
 * @file
 * Contains \Drupal\custom_twig_extensions\Service.
 */

namespace Drupal\custom_twig_extensions\Service;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;



class CustomTwigExtension extends AbstractExtension
{

    /**
     * {@inheritdoc}
     * This function must return the name of the extension. It must be unique.
     */
    public function getName()
    {
        return 'custom_twig_extension';
    }

    /**
     * In this function we can declare the extension function
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('get_terms_taxonomie', [$this, 'getTermsTaxonomie']),
            new TwigFunction('truncate_text', [$this, 'getTruncateText']),
            new TwigFunction('convert_to_embed_url', [$this, 'convertToEmbedUrl']),
            new TwigFunction('truncate_text', [$this, 'truncateText']),
        ];
    }


    public function getTermsTaxonomie($vocabulary_name)
    {
        if (!$vocabulary_name) return;
        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => $vocabulary_name]);
        return $terms;
    }

    function convertToEmbedUrl($url, $loop = false)
    {
        $videoId = '';
        $pattern = '#^(?:https?://)?(?:www\.)?(?:youtube\.com/watch\?v=|youtu\.be/|youtube\.com/embed/|youtube\.com/v/|youtube\.com/watch\?feature=player_embedded&v=)([A-Za-z0-9\-_]+)#';
        if (preg_match($pattern, $url, $matches)) {
            $videoId = $matches[1];
        }

        $basePath = 'https://www.youtube.com/embed/' . $videoId;

        if ($loop) {
            return $basePath . '?playlist=' . $videoId . '&loop=1&autoplay=1&mute=1&rel=1';
        }

        return $basePath;
    }

    function truncateText($text, $wordCount)
    {
        $words = explode(' ', $text);
        if (count($words) > $wordCount) {
            $truncatedText = implode(' ', array_slice($words, 0, $wordCount));
            $truncatedText .= '...';
            return $truncatedText;
        }
        return $text;
    }
}
